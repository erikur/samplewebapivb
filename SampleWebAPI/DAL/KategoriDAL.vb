﻿Imports System.Data.SqlClient
Imports Dapper

Public Class KategoriDAL
    Private Function GetConnString() As String
        Return ConfigurationManager.ConnectionStrings("MyConnectionString").ConnectionString
    End Function

    Public Function GetAll() As IEnumerable(Of Kategori)
        Using conn As New SqlConnection(GetConnString())
            Dim strSql = "select * from Kategori order by NamaKategori asc"
            Dim results = conn.Query(Of Kategori)(strSql)
            Return results
        End Using
    End Function

    Public Function GetByID(id As Integer) As Kategori
        Using conn As New SqlConnection(GetConnString())
            Dim strSql = "select * from Kategori where KategoriID=@KategoriID"
            Dim param = New With {.KategoriID = id}
            Dim result = conn.QuerySingleOrDefault(Of Kategori)(strSql, param)
            Return result
        End Using
    End Function

    Public Sub Insert(kat As Kategori)
        Using conn As New SqlConnection(GetConnString())
            Dim strSql = "insert into Kategori(NamaKategori) values(@NamaKategori)"
            Dim param = New With {.NamaKategori = kat.NamaKategori}
            Try
                conn.Execute(strSql, param)
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Sub

    Public Sub Update(id As Integer, kat As Kategori)
        Using conn As New SqlConnection(GetConnString())
            Dim strSql = "update Kategori set NamaKategori=@NamaKategori,Count=@Count where KategoriID=@KategoriID"
            Dim param = New With {
                .NamaKategori = kat.NamaKategori,
                .Count = kat.Count,
                .KategoriID = id}
            Try
                conn.Execute(strSql, param)
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Sub

    Public Sub Delete(id As Integer)
        Using conn As New SqlConnection(GetConnString())
            Dim strSql = "delete Kategori where KategoriID=@KategoriID"
            Dim param = New With {
                .KategoriID = id}
            Try
                conn.Execute(strSql, param)
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Sub
End Class
