﻿Imports System.Net
Imports System.Web.Http

Namespace Controllers
    Public Class KategoriController
        Inherits ApiController

        Private kategoriDAL As New KategoriDAL

        ' GET: api/Kategori
        Public Function GetValues() As IEnumerable(Of Kategori)
            Dim results = kategoriDAL.GetAll()
            Return results
        End Function

        ' GET: api/Kategori/5
        Public Function GetValue(ByVal id As Integer) As Kategori
            Dim result = kategoriDAL.GetByID(id)
            Return result
        End Function

        ' POST: api/Kategori
        Public Function PostValue(<FromBody()> objKat As Kategori) As IHttpActionResult
            Try
                kategoriDAL.Insert(objKat)
                Return Ok("Data berhasil ditambahkan")
            Catch ex As Exception
                Return BadRequest("Error :" & ex.Message)
            End Try
        End Function

        ' PUT: api/Kategori/5
        Public Function PutValue(ByVal id As Integer, <FromBody()> kat As Kategori) As IHttpActionResult
            Try
                kategoriDAL.Update(id, kat)
                Return Ok($"Data berhasil {kat.NamaKategori} diupdate")
            Catch ex As Exception
                Return BadRequest("Error :" & ex.Message)
            End Try
        End Function

        ' DELETE: api/Kategori/5
        Public Function DeleteValue(ByVal id As Integer) As IHttpActionResult
            Try
                kategoriDAL.Delete(id)
                Return Ok($"Data berhasil di delete")
            Catch ex As Exception
                Return BadRequest("Error :" & ex.Message)
            End Try
        End Function
    End Class
End Namespace